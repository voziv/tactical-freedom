USE [tacticalfreedom]
GO

/****** Object:  StoredProcedure [dbo].[gt_prc_add_game_data]    Script Date: 2/11/2015 5:19:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[gt_prc_add_game_data]
(
	@game_id int,
	@time datetime,
	@client_id smallint,
	@steam_id int,
	@name varchar(100),
	@team tinyint,
	@is_alive bit,
	@kills smallint,
	@deaths smallint,
	@x float,
	@z float
)
as begin

	declare @existing_client_id int = (select client_id from gt_player where game_id = @game_id and client_id = @client_id)

	if @existing_client_id is null begin
		insert into gt_player (game_id, client_id, steam_id, name) values (@game_id, @client_id, @steam_id, @name)
	end
	
	declare @time_id smallint = (select time_id from gt_time where game_id = @game_id and timestamp = @time)
	declare @max_time_id smallint = (select max(time_id) from gt_time where game_id = @game_id)

	if (@max_time_id is null) begin
		set @time_id = 0
		insert into gt_time (time_id, game_id, timestamp) values (@time_id, @game_id, @time)
	end else begin
		if (@time_id is null) begin
			set @time_id = @max_time_id + 1
			insert into gt_time (time_id, game_id, timestamp) values (@time_id, @game_id, @time)
		end
	end

	insert into gt_gamedata
	(game_id, time_id, client_id, team, is_alive, kills, deaths, x, z)
	values
	(@game_id, @time_id, @client_id, @team, @is_alive, @kills, @deaths, @x, @z)

end




GO

