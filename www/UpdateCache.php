<?php

function IDfrom64($steamId64) {
    $iServer = "1";
    if(bcmod($steamId64, "2") == "0") {
        $iServer = "0";
    }
    $steamId64 = bcsub($steamId64,$iServer);
    if(bccomp("76561197960265728",$steamId64) == -1) {
        $steamId64 = bcsub($steamId64,"76561197960265728");
    }
    $steamId64 = bcdiv($steamId64, "2");
    if (strpos($steamId64, ".")) {
            $steamId64=strstr($steamId64,'.', true);
        }     
    return ($steamId64 * 2) + $iServer;
}

$users = array();

$url = 'http://steamcommunity.com/groups/TacticalFreedom/memberslistxml/';

//$xml = simplexml_load_file($url);

$ch = curl_init();
$timeout = 5; // set to zero for no timeout
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$result = curl_exec($ch);
curl_close($ch);

$xml = simplexml_load_string($result);

$steamids = $xml->members->steamID64;
$members = $xml->memberCount;
$row = 0;
while($row < $members)
	{
	/*
	//$xmlID = 'http://steamcommunity.com/profiles/' . $steamids[$row] . '/?xml=1';
	//$xmlID = simplexml_load_file($xmlID);

	$url = 'http://steamcommunity.com/profiles/' . $steamids[$row] . '/?xml=1';
	$ch = curl_init();
	$timeout = 2; // set to zero for no timeout
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	$result = curl_exec($ch);
	curl_close($ch);

	$result = str_replace("<![CDATA[","",$result);
	$result = str_replace("]]>","",$result);

	$xmlID = simplexml_load_string($result);
	*/

	$ns2id = IDfrom64($steamids[$row]);
	
	//$name = (string) $xmlID->steamID;
	$name = (string) $steamids[$row];

	$group = null;

	if (in_array($ns2id, array("59292312")))
    {
        $group = 'Officer-Donor';
    }
    elseif (in_array($ns2id, array("205659","198230")))
	{
		$group = 'Officer';
	}
    elseif (in_array($ns2id, array("31001003","35158890","17917222","152115813","31402288","51620782")))
    {
        $group = 'Moderator-Donor';
    }
	elseif (in_array($ns2id, array("28221725","6638152","29829661","111557539","1371407","32678528","12470919")))
	{
		$group = 'Moderator';
	}
    elseif (in_array($ns2id, array("000000")))
    {
        $group = 'Member-Donor';
    }
	else
	{
		$group = 'Member';
	}

	$users[$ns2id] = array(
		'Name' => $name,
		'Group' => $group
	);

	$row += 1;
}

$data = (object)array(
        'Groups' => array(
        	'Officer' => array(
        		'Badges' => array('tf-officer','tf-moderator','tf-member'),
    			'Commands' => array('sh_afk','sh_randomimmune','sh_balanceimmune','sh_donor'),
    			'IsBlacklist' => true,
    			'Immunity' => 20,
                'CanAlwaysTarget' => true
        	),
            'Officer-Donor' => array(
                'Badges' => array('tf-donor','tf-officer','tf-moderator','tf-member'),
                'Commands' => array('sh_afk','sh_randomimmune','sh_balanceimmune'),
                'IsBlacklist' => true,
                'Immunity' => 20,
                'CanAlwaysTarget' => true
            ),
    		'Moderator' => array(
    			'Badges' => array('tf-moderator','tf-member'),
    			'Commands' => array('sh_afk','sh_randomimmune','sh_balanceimmune','sh_unban','sh_reset','sh_verify','sh_donor'),
    			'IsBlacklist' => true,
    			'Immunity' => 15,
                'CanAlwaysTarget' => true
    		),
            'Moderator-Donor' => array(
                'Badges' => array('tf-donor','tf-moderator','tf-member'),
                'Commands' => array('sh_afk','sh_randomimmune','sh_balanceimmune','sh_unban','sh_reset','sh_verify'),
                'IsBlacklist' => true,
                'Immunity' => 15,
                'CanAlwaysTarget' => true
            ),
    		'Member' => array(
    			'Badges' => array('tf-member'),
    			'Commands' => array('sh_member','sh_togglebots','sh_afk_partial','sh_reservedslot','sh_forcebalance','sh_ignorestatus','sh_pm'),
    			'IsBlacklist' => false,
    			'Immunity' => 5,
                'CanAlwaysTarget' => true
    		),
            'Member-Donor' => array(
                'Badges' => array('tf-donor','tf-member'),
                'Commands' => array('sh_member','sh_togglebots','sh_afk_partial','sh_reservedslot','sh_forcebalance','sh_ignorestatus','sh_pm','sh_donor'),
                'IsBlacklist' => false,
                'Immunity' => 5,
                'CanAlwaysTarget' => true
            )
    	),
    	'DefaultGroup' => array(
	    	'Commands' => array('sh_togglebots','sh_afk_partial'),
	    	'IsBlacklist' => false,
	    	'Immunity' => 0
    	),
    	'Users' => $users
    );

$json = json_encode($data, JSON_PRETTY_PRINT);

file_put_contents('UserConfig.cache',$json);

?>