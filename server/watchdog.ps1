[CmdletBinding()]
Param(
    [Parameter(ValueFromPipelineByPropertyName=$true)] $serverid = $null
)

$script_path = Split-Path -Parent $MyInvocation.MyCommand.Definition

If (-not (Test-Path "$script_path\secrets.json"))
{
    Write-Output "","[ERROR] secrets.json not found.",""
    exit
}

$secrets = Get-Content "$script_path\secrets.json" | Out-String | ConvertFrom-Json
$servers = Get-Content "$script_path\servers.json" | Out-String | ConvertFrom-Json

While($true)
{
    $timestamp = Get-Date -Format g

    ForEach($server in $servers)
    {
        if ($serverid -ne $null)
        {
            if ($serverid -ne $server.serverid)
            {
                continue
            }
        }

        $serverid = $server.serverid

        $servername = $server._name
        $install_dir = $server.install_dir
        $webdomain = $server.arguments.webdomain
        $webport = $server.arguments.webport
        $server_secrets = $null

        $logfile = "watchdog.$servername.log"

        ForEach($ss in $secrets.servers)
        {
            If ($ss.serverid -eq $serverid)
            {
                $server_secrets = $ss
                break
            }
        }

        $webuser = $server_secrets.webuser
        $webpassword = $server_secrets.webpassword

        $process = Get-Process | Where-Object Path -like $install_dir\*
        
        if ($process -eq $null)
        {
            $warning = "[WARNING] Restarting $servername due to process not found..."
            Write-Output "$timestamp `t $warning"
            Add-Content "$logfile" "$timestamp `t $warning"
            .\condenser.ps1 -launch -serverid $serverid
            continue
        }
        elseif ($process.count -gt 1)
        {
            $warning = "[WARNING] Restarting $servername due to multiple processes found"
            Write-Output "$timestamp `t $warning"
            Add-Content "$logfile" "$timestamp `t $warning"
            
            ForEach($p in $process)
            {
                Stop-Process -Force $p.Id
            }

            .\condenser.ps1 -launch -serverid $serverid
            continue
        }
        else
        {
            $path = $process | Select-Object Path | Split-Path
            $uptime = New-TimeSpan -Start ($process | Select-Object StartTime).StartTime
            $responding = $process | Select-Object Responding
            $window = $process | Select-Object MainWindowHandle
        }

        if (($responding -eq $false) -or ($window -eq 0))
        {
            $warning = "[WARNING] Restarting $servername due to not responding"
            Write-Output "$timestamp `t $warning"
            Add-Content "$logfile" "$timestamp `t $warning"
            
            Stop-Process -Force $process.Id
            .\condenser.ps1 -launch -serverid $serverid
            continue
        }

        $webclient = new-object System.Net.WebClient
        $webclient.Credentials = new-object System.Net.NetworkCredential($webuser, $webpassword)
        $url = "http://${webdomain}:${webport}/?command=Send&rcon=sv_status"
        
        try
        {
            $response = $webclient.DownloadString($url)

            $status = $response | ConvertFrom-Json
            $maptime = $status.uptime
            $player_count = $status.player_list.Count

            If ($player_count -lt 2)
            {
                if ($maptime -gt (60*20))
                {
                    $hour_of_day = (Get-Date).Hour
                        
                    if (($uptime.TotalHours -gt 120) -and ($hour_of_day -eq 6))
                    {
                        $warning = "[WARNING] Restarting $servername due to extended uptime ({0}hrs)" -f [math]::Round($uptime.TotalHours)
                        Write-Output "$timestamp `t $warning"
                        Add-Content "$logfile" "$timestamp `t $warning"
                        Stop-Process -Force $process.Id
                        .\condenser.ps1 -launch -serverid $serverid
                    }
                    else
                    {
                        $warning = "[WARNING] Cycling map on $servername due to extended maptime ({0}min)" -f [math]::Round($maptime/60)
                        Write-Output "$timestamp `t $warning"
                        Add-Content "$logfile" "$timestamp `t $warning"
                        $webclient = new-object System.Net.WebClient
                        $webclient.Credentials = new-object System.Net.NetworkCredential($webuser, $webpassword)
                        $url = "http://${webdomain}:${webport}/?command=Send&rcon=cyclemap"
                        $response = $webclient.DownloadString($url)
                    }
                }
                else
                {
                    $info = "[INFO] $servername appears healthy - $player_count players active"
                    Write-Output "$timestamp `t $info"
                    Add-Content "$logfile" "$timestamp `t $info"
                }
            }
            else
            {
                $info = "[INFO] $servername appears healthy - $player_count players active"
                Write-Output "$timestamp `t $info"
                Add-Content "$logfile" "$timestamp `t $info"
            }
        }
        catch [Exception]
        {
            $message = $_.Exception.Message

            if ($message.Contains("Invalid JSON primitive"))
            {
                $warning = "[WARNING] $servername got message $message. Ignoring."
                Write-Output "$timestamp `t $warning"
                Add-Content "$logfile" "$timestamp `t $warning"
            }
            else
            {
                $warning = "[WARNING] Restarting $servername due to error: {0}" -f $message
                Write-Output "$timestamp `t $warning"
                Add-Content "$logfile" "$timestamp `t $warning"
                
                Stop-Process -Force $process.Id
                .\condenser.ps1 -launch -serverid $serverid
            }
        }
    }

    Start-Sleep -s 60
}