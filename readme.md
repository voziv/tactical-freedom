# README #

This repo houses the Tactical Freedom NS2 server configuration and the [tacticalfreedom.com](http://tacticalfreedom.com) website source

### What is this repository for? ###

* **/server:** ns2 server configuration using [condenser](https://github.com/ShamelessCookie/condenser)
* **/tracker:** tracker.tacticalfreedom.com API, database, and http source
* **/www:** tacticalfreedom.com source