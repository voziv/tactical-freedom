USE [tacticalfreedom]
GO

/****** Object:  StoredProcedure [dbo].[gt_prc_get_game_data]    Script Date: 2/11/2015 5:19:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[gt_prc_get_game_data]
(
	@game_id int
)
as begin

	select * from gt_game
	where game_id = @game_id

	select client_id, steam_id, name from gt_player
	where game_id = @game_id
	
	select time_id, client_id, team, is_alive, kills, deaths, x, z  from gt_gamedata
	where game_id = @game_id
	order by time_id asc

end


GO

