<?php

if (file_exists('UserConfig.cache'))
{
	$cache = file_get_contents('UserConfig.cache');

	if (isset($_GET['pretty']))
	{
		echo '<pre>' . $cache . '</pre>';
	}
	else
	{
		echo $cache;

		$ch = curl_init();
 
		curl_setopt($ch, CURLOPT_URL, 'http://tacticalfreedom.com/UpdateCache.php');
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		 
		curl_exec($ch);
		curl_close($ch);
	}
}

?>