var secrets = require('./secrets.json');
var tools = require('./tools');
var moment = require('moment');

var fs = require('fs');
var sql = require('mssql');
var express = require('express');
var bodyparser = require('body-parser');

var app = express();

var sqlconfig = {
    user: secrets.sqluser,
    password: secrets.sqlpassword,
    server: secrets.sqlserver,
    database: secrets.sqldatabase,
    
    options: {
        encrypt: true
    }
}

var jsonParser = bodyparser.json();
var urlencodedParser = bodyparser.urlencoded({ extended: true });
 
app.post('/api/Game', urlencodedParser, function(req, res) {
	console.log(req.body);

	var server_name = req.body.ServerName;
	var map = req.body.Map;

	if (!server_name) {
		res.status(400).send(tools.BadRequest("ServerName"));
		console.log(req.body);
		return;
	}

	if (!map) {
		res.status(400).send(tools.BadRequest("Map"));
		console.log(req.body);
		return;
	}

	var sqlconnection = new sql.Connection(sqlconfig, function(err) {
	    var request = new sql.Request(sqlconnection);
	    request.input('server_name', sql.VarChar(100), server_name);
	    request.input('map', sql.VarChar(37), map);
	    request.execute('gt_prc_create_new_game', function(err, recordset, returnValue) {
	        if (err) {
	        	res.sendStatus(500);
	        	console.log(req.body);
	        	console.log(err);
	        	return;
	        } else {
	        	console.log(recordset[0][0]);
	        	var game_id = recordset[0][0].game_id;
	        	res.send(JSON.stringify({ 'GameId': game_id }));
	        	return;
	        }
	    });
	});
});

app.post('/api/GameData', urlencodedParser, function(req, res) {
	var game_id = req.body.GameId;
	var time = new Date();
	var players = JSON.parse(req.body.PlayerData);

	var timeString = time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();

	console.log('GameId: ' + game_id + ', Time: ' + timeString + ', PlayerCount: ' + players.length);

	var successcount = 0;

	var sqlconnection = new sql.Connection(sqlconfig, function(sqlerr) {

		for(var p = 0; p < players.length; p++) {
			var player = players[p];

			var client_id = player['ClientId'];
			var steam_id = player['SteamId'];
			var name = player['Name'];
			var team = player['Team'];
			var is_alive = player['IsAlive'];
			var kills = player['Kills'];
			var deaths = player['Deaths'];
			var x = player['X'];
			var z = player['Z'];

		    var request = new sql.Request(sqlconnection);
		    request.input('game_id', sql.VarChar(37), game_id);
		    request.input('time', sql.DateTime, time);
		    request.input('client_id', sql.SmallInt, client_id);
		    request.input('steam_id', sql.Int, steam_id);
		    request.input('name', sql.VarChar(100), name);
		    request.input('team', sql.TinyInt, team);
		    request.input('is_alive', sql.Bit, is_alive);
		    request.input('kills', sql.SmallInt, kills);
		    request.input('deaths', sql.SmallInt, deaths);
		    request.input('x', sql.Float, x);
		    request.input('z', sql.Float, z);
		    request.execute('gt_prc_add_game_data', function(prcerr, recordset, returnValue) { 
		    	if (prcerr) {
		        	console.log(prcerr);
		        }
		    });
		}

	});

	res.sendStatus(200);
	return;
	
});

app.get('/api/GameData', urlencodedParser, function(req, res) {
	console.log(req.query);

	var game_id = req.query.GameId;

	if (!game_id) {
		res.status(400).send(tools.BadRequest("GameId"));
		console.log(req.query);
		return;
	}

	var sqlconnection = new sql.Connection(sqlconfig, function(err) {
	    var request = new sql.Request(sqlconnection);
	    request.input('game_id', sql.VarChar(100), game_id);
	    request.execute('gt_prc_get_game_data', function(err, recordset, returnValue) {
	        if (err) {
	        	res.sendStatus(500);
	        	console.log(req.query);
	        	console.log(err);
	        	return;
	        } else {
	        	var match_data = recordset[0][0];

	        	if (match_data == null) {
	        		res.sendStatus(404);
		        	console.log(req.query);
		        	return;
	        	}

	        	var player_data = recordset[1];
	        	var position_data = recordset[2];

	        	var game_data = {
	        		G: match_data.game_id,
	        		S: match_data.server_name,
	        		M: match_data.map,
	        		T: match_data.time_started
	        	};

	        	game_data.Players = new Array();

	        	for(var p = 0; p < player_data.length; p++) {
	        		var player_row = player_data[p];

	        		var data_row = {
	        			C: player_row.client_id,
	        			S: player_row.steam_id,
	        			N: player_row.name
	        		};

	        		game_data.Players.push(data_row);
	        	}

	        	game_data.Positions = {}

	        	for(var r = 0; r < position_data.length; r++) {
	        		var position_row = position_data[r];

	        		var time_id = position_row.time_id;

	        		var alive_int = 0;

	        		if (position_row.is_alive) { alive_int = 1; }
	        		
	        		var data_row = {
	        			C: position_row.client_id,
	        			T: position_row.team,
	        			A: alive_int,
	        			K: position_row.kills,
	        			D: position_row.deaths,
	        			X: position_row.x,
	        			Z: position_row.z
	        		};
	        		
	        		if ("undefined" != typeof game_data.Positions[time_id]) {
	        			game_data.Positions[time_id].push(data_row);
	        		}
	        		else {
	        			game_data.Positions[time_id] = new Array();
	        			game_data.Positions[time_id].push(data_row);
	        		}
	        	}

				res.header('Access-Control-Allow-Origin', 'http://tracker.tacticalfreedom.com');
	        	res.send(JSON.stringify(game_data));
	        	return;
	        }
	    });
	});

});

app.listen(process.env.PORT || 58423);