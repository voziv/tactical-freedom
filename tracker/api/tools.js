module.exports = {
	BadRequest: function (param) {
		var errorMessage = "Parameter \"" + param + "\" is required.";
		console.log(errorMessage);
		return errorMessage;
	}
}