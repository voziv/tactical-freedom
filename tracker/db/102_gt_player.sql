USE [tacticalfreedom]
GO

/****** Object:  Table [dbo].[gt_player]    Script Date: 2/11/2015 5:18:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gt_player](
	[game_id] [int] NOT NULL,
	[client_id] [smallint] NOT NULL,
	[steam_id] [int] NOT NULL,
	[name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_gt_player_1] PRIMARY KEY CLUSTERED 
(
	[game_id] ASC,
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

