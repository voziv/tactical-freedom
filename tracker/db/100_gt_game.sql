USE [tacticalfreedom]
GO

/****** Object:  Table [dbo].[gt_game]    Script Date: 2/11/2015 5:18:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gt_game](
	[game_id] [int] IDENTITY(10000,1) NOT NULL,
	[server_name] [varchar](100) NOT NULL,
	[time_started] [datetime] NOT NULL,
	[map] [varchar](37) NOT NULL,
 CONSTRAINT [PK_gt_game] PRIMARY KEY CLUSTERED 
(
	[game_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

