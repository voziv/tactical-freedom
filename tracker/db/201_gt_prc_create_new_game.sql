USE [tacticalfreedom]
GO

/****** Object:  StoredProcedure [dbo].[gt_prc_create_new_game]    Script Date: 2/11/2015 5:19:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[gt_prc_create_new_game]
(
	@server_name varchar(100),
	@map varchar(37)
)
as begin

	insert into gt_game	(server_name, time_started, map)
	values (@server_name, getdate(), @map)

	select SCOPE_IDENTITY() as [game_id]
end


GO

