USE [tacticalfreedom]
GO

/****** Object:  Table [dbo].[gt_time]    Script Date: 2/11/2015 5:19:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[gt_time](
	[game_id] [int] NOT NULL,
	[time_id] [smallint] NOT NULL,
	[timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_gt_time] PRIMARY KEY CLUSTERED 
(
	[time_id] ASC,
	[game_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

