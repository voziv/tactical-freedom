USE [tacticalfreedom]
GO

/****** Object:  Table [dbo].[gt_gamedata]    Script Date: 2/11/2015 5:18:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[gt_gamedata](
	[game_id] [int] NOT NULL,
	[time_id] [int] NOT NULL,
	[client_id] [smallint] NOT NULL,
	[team] [tinyint] NOT NULL,
	[is_alive] [bit] NOT NULL,
	[kills] [smallint] NOT NULL,
	[deaths] [smallint] NOT NULL,
	[x] [float] NOT NULL,
	[z] [float] NOT NULL,
 CONSTRAINT [PK_gt_gamedata] PRIMARY KEY CLUSTERED 
(
	[game_id] ASC,
	[time_id] ASC,
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

